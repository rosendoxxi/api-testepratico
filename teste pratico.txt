Crie uma WebApi em C#, utilizando ou .NET Framework ou .NET Core.
•	A API deve acessar um banco de dados (a ser criado por você) do SQL Server e ser capaz de armazenar, ler e manipular os dados dele. Este banco deve ser criado utilizando Code First.
•	A API deve disponibilizar as funções do CRUD.
•	A API só deve aceitar requisições HTTPS.
•	A API deve possuir autenticação (preferencialmente OAuth 2).

Crie uma aplicação utilizando Angular 7 ou superior para consumir todas as funções da API.

Use sua criatividade.
Utilize boas práticas de programação orientada a objetos. 

Após concluir, disponibilize a aplicação (front-end e back-end) em um repositório do Github e informe abaixo a URL.

Devemos conseguir fazer o clone do repositório, criar o banco de dados com code-first e compilar a aplicação no Visual Studio 2019 para validação do teste. Caso necessite alimentar o banco de dados com algum dado inicial, disponibilize um arquivo na raiz do repositório com o script SQL a ser executado após a criação do banco de dados no SQL Server.